 <?php
 if(isset($_COOKIE["id"]))
 {
	 $id = $_COOKIE["id"];
	 
	 if(isset($_GET["join"])) {
		$join = $_GET["join"];
	 } else {
		$join = false; 
	 }
	 require_once 'htmlfunc.php';
	 require_once(__DIR__."/../lib/twilio.php");
	 require_once(__DIR__."/../lib/evernoteSignIn.php");

	function inDemo($id, $join) {
		$convoID = "2";
		$conversations = getUserConversations($id);
		//echo $id;
		//var_dump($conversations);
		if((array_search($convoID, $conversations)) === false) {
			if($join == true) {
				sendToConversation($convoID, getUserName($id). " has been added to the group chat.");
				joinConversation($convoID, $id);
				sendMessage(getConversationNumber($convoID),array($id), "You have been added to group chat " . getConversationName($convoID) . ". Text @helps for a list of available commands.");
				echo "<h4>Congratulations</h4><br/>
					<p> You've been added to the demo conversation.</p>";
			} else {
				echo "<h4>Huh</h4><br/>
					<p> Looks like you're part of the demo conversation. Would you like to join?
						<a href='splash.php?join=true'>click here.</a></p>";
			}
			return false;
		}
		return true;
	}
	 
	 function wantAuthKey() {
	  global $id;
	  $api = getUserEvernote($id);
	  //echo $id;
	  //echo $api;
	  if (empty($api)) { ?>
		<h4>Uh-Oh</h4><br/>
		<p> Looks like we need your authorization key to integrate EverNote with your account. We've made it extremely
		  easy for you. Just <a href="<?php echo generateEvernoteButton(); ?>">click here.</a> </p>

	  <?php
	  } 
	 }
	 insertHeader("My Account | EverTexts", '<link href="css/splash.css" rel="stylesheet">');
	 insertNav("enter");
	 $demo = inDemo($id, $join);
	 wantAuthKey();
	 
	 ?>

	 <body>
	  <h3> Account Information
	  </h3> <br/>
	  <p>
		Here are all of your past messages from your current conversation.
		<div class="message" id="" style=""> 
		<?php 
			if($demo) {
				echo export($id);
			}
			//$messages
		?>

		</div>
	  </p>
	<?php
	  insertFooter();
	  insertEndTags();
  } else {
	echo '<META http-equiv="refresh" content="0;URL=../webapp">';
  }
  
?>